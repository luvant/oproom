# Oproom: reserve a room

Oproom is a platform where teams and organizations can define their resources and optimally share them among its members by reserving it for a specific period of time. As a context, we aim to create a tool for hospitals to efficiently manage the operating theaters by reserving when they are available, checking their current status, and sending notifications to subscribed users when room status updates.

We aim to enable the users to create rooms and to reserve them through our API. The system is composed by several microservices:

- storage service (io-microservice): stores data about the users, rooms and reservations
- auth service: enables an user to create an account, to login and logout (by generating and using access tokens)
- rooms service: enables an authenticated user to find or create a room (according to its permissions)
- reservation service: enables an authenticated user to reserve an existing room (according to its permissions)

For these, others actors are involved:

- kong: managing the interaction of the external world with the microservices
- swarmpit: enables us to interact with the swarm through a web interface
- mongoclient: enables us to interact with the storage through a web interface

## Init docker swarm on manager node

```bash
root@nodemanager:/home/manager# docker swarm init
```

## Join the swarm (from workers)

```bash
root@nodeworker1:/home/worker1# docker swarm join --token SWMTKN-1-5hipmz3zjz7dpf97uvsz67i3ee9rmy85r9jfsvv2so6pq1d810-2l434509jx2ldf7uwdz3x3gke 13.82.173.87:2377
```

## Integration and development

As a first step for a CI/CD, we prepared a set of instructions to be executed in order to manage the deployments.

When commit on master branch

### Build image

```bash
docker build -t <docker-hub-repo>/<microservice-name>:${GIT-COMMIT} .
docker build -t <docker-hub-repo>/<microservice-name>:latest .
```

To efficiently track the version of the docker images, we use the git commit id as build tag. Moreover, to keep consistent and always up-to-date, we generate a docker image with the tag `latest` so we know that everytime a microservice is deplyed, the latest version is pulled.

### Push image

```bash
docker push <docker-hub-repo>/<microservice-name>:${GIT-COMMIT}
docker push <docker-hub-repo>/<microservice-name>:latest
```

## Running the stacks

```bash
docker run -d -p 2049:2049 --name nfs --privileged -v ~/nfs:/nfsshare -e SHARED_DIRECTORY=/nfsshare itsthenetwork/nfs-server-alpine:latest
docker stack deploy -c stack.yml op
```

```bash
docker stack deploy -c swarmpit.yml swarmpit
```

For more details, go to <https://dockerswarm.rocks/swarmpit/>

## Connect to nodes

Node Manager - IP Address: 45.55.35.91

Node 1 - IP Address: 104.131.113.198

Node 2 - IP Address: 167.71.188.220
