const express = require("express");
const router = express.Router();
const hasRole = require("../middleware/hasRole");
const {
  createRoom,
  getRooms,
  getRoomById,
  updateRoom,
  deleteRoom,
} = require("../services/roomService");
const { validationRules, validate } = require("../middleware/validator");
const accessControl = require("../middleware/accessControl");

/**
 * Get by query
 */
router.get("/", hasRole(["manager", "user"]), async (req, res, next) => {
  try {
    let result = await getRooms(req.query);
    res.send(result);
  } catch (error) {
    next(error);
  }
});

/**
 * Get by id
 */
router.get("/:id", hasRole(["manager", "user"]), async (req, res, next) => {
  try {
    let result = await getRoomById(req.params.id);
    res.send(result);
  } catch (error) {
    next(error);
  }
});

/**
 * Create new room
 */
router.post(
  "/",
  hasRole("manager"),
  validationRules("create"),
  validate,
  async (req, res, next) => {
    try {
      const ownerId = req.user._id;
      let result = await createRoom({ ownerId, ...req.body });
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * Update room
 */
router.put(
  "/:id",
  hasRole("manager"),
  accessControl(),
  async (req, res, next) => {
    try {
      let result = await updateRoom(req.params.id, req.body);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * Delete room
 */
router.delete(
  "/:id",
  hasRole("manager"),
  accessControl(),
  async (req, res, next) => {
    try {
      let result = await deleteRoom(req.params.id);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;
