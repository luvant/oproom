const Router = require("express").Router();

const roomControllers = require("../controllers/roomControllers");

Router.use("/rooms", roomControllers);

module.exports = Router;
