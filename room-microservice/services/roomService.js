const axios = require("axios");
const config = require("../config.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");


/**
 * Get operation rooms by query
 */
const getRooms = async (query) => {
  let result = await axios.get(`${config.ioServiceHost}/rooms`, { params: query });
  return result.data;
}

/**
 * Get room by id
 */
const getRoomById = async (id) => {
  let result = await axios.get(`${config.ioServiceHost}/rooms/${id}`);
  return result.data;
}

/**
 * Create a operation room
 */
const createRoom = async (body) => {
  let result = await axios.post(`${config.ioServiceHost}/rooms`, body);
  return result.data;
};

/**
 * Update room 
 */
const updateRoom = async (id, body) => {
  let result = await axios.put(`${config.ioServiceHost}/rooms/${id}`, body);
  return result.data;
}

/**
 * Delete room
 */
const deleteRoom = async (id) => {
  let result = await axios.delete(`${config.ioServiceHost}/rooms/${id}`);
  return result.data;
}
module.exports = {
  createRoom,
  getRooms,
  getRoomById,
  updateRoom,
  deleteRoom
};
