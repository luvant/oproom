const config = require("../config");
const { default: axios } = require("axios");

/**
 * Auth middleware
 */
const hasRole = (role) => async (req, res, next) => {
  try {
    // Verification token and get user
    const token = req.header("Authorization").replace("Bearer ", "");
    let result = await axios.get(`${config.auServiceHost}/auth/me`, {
      headers: {
        Authorization: req.header("Authorization"),
      },
    });
    let user = result.data;

    // Check if user is manager
    if (role instanceof Array ? !role.includes(user.role) : user.role !== role) throw new Error();

    req.user = user;
    req.token = token;
    next();
  } catch (error) {
    res.status(401).send({ error: "Not authorized to access this resource" });
  }
};
module.exports = hasRole;
