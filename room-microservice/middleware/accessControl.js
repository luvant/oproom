const config = require("../config");
const { default: axios } = require("axios");

/**
 * Auth middleware
 */
const accessControl = () => async (req, res, next) => {
  try {
    let roomId = req.params.id;

    // Get the room
    roomData = await axios.get(`${config.ioServiceHost}/rooms/${roomId}`);
    roomData = roomData.data;

    // Check if the room is owned by the user
    if (roomData.ownerId !== req.user._id) {
      throw new Error();
    }
    next();
  } catch (error) {
    res.status(401).send({ error: "Not authorized to access this resource" });
  }
};
module.exports = accessControl;
