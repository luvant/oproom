const { body, validationResult } = require("express-validator");
const validationRules = (type) => {
  switch (type) {
    case "create":
      return [
        body("name")
          .exists()
          .withMessage("room name is required")
          .isLength({ min: 3, max: 128 })
          .withMessage("room name must be at least 5 chars long"),
        body("address")
          .exists()
          .withMessage("address is required")
          .isLength({ min: 3, max: 128 })
          .withMessage("address must be at least 5 chars long"),
      ];

    default:
      return [
        body("name")
          .isLength({ min: 3, max: 128 })
          .withMessage("room name must be at least 5 chars long"),
        body("address")
          .isLength({ min: 3, max: 128 })
          .withMessage("address must be at least 5 chars long"),
      ];
  }
};

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(422).json({
    errors: extractedErrors,
  });
};

module.exports = {
  validationRules,
  validate,
};
