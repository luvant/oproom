const config = {
  port: process.env.EXPOSE_PORT ? process.env.EXPOSE_PORT : "3003",
  ioServiceHost: process.env.IO_SERVICE_HOST
    ? process.env.IO_SERVICE_HOST
    : "http://localhost:3001/api",
  auServiceHost: process.env.AUTH_SERVICE_HOST
    ? process.env.AUTH_SERVICE_HOST
    : "http://localhost:3002/api",
};
module.exports = config;
