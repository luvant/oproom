const express = require("express");
const router = express.Router();
const hasRole = require("../middleware/hasRole");
const accessControl = require("../middleware/accessControl");
const {
  createReservation,
  getReservations,
  getReservationById,
  updateReservation,
  deleteReservation,
} = require("../services/reservationService");
const { validationRules, validate } = require("../middleware/validator");

/**
 * Get by query
 */
router.get("/", hasRole(["user", "manager"]), async (req, res, next) => {
  try {
    let opRooms = await getReservations(req);
    res.send(opRooms);
  } catch (error) {
    next(error);
  }
});

/**
 * Get by id
 */
router.get(
  "/:id",
  hasRole(["user", "manager"]),
  accessControl(),
  async (req, res, next) => {
    try {
      let opRoom = await getReservationById(req.params.id);
      res.send(opRoom);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * Create new reservation
 */
router.post(
  "/",
  hasRole(["user", "manager"]),
  validationRules("create"),
  validate,
  async (req, res, next) => {
    try {
      let userId = req.user._id;
      let reservation = await createReservation({
        userId,
        ...req.body,
      });
      res.send(reservation);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * Update reservation
 */
router.put(
  "/:id",
  hasRole(["user", "manager"]),
  accessControl(),
  validationRules("update"),
  validate,
  async (req, res, next) => {
    try {
      let reservation = await updateReservation(req.params.id, req.body);
      res.send(reservation);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * Delete reservation
 */
router.delete(
  "/:id",
  hasRole(["manager", "user"]),
  accessControl(),
  async (req, res, next) => {
    try {
      let result = await deleteReservation(req.params.id);
      res.send(result);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;
