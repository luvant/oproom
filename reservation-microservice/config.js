const config = {
  port: process.env.EXPOSE_PORT ? process.env.EXPOSE_PORT : "3004",
  ioServiceHost: process.env.IO_SERVICE_HOST
    ? process.env.IO_SERVICE_HOST
    : "http://localhost:3001/api",
  authHost: process.env.AUTH_SERVICE_HOST
    ? process.env.AUTH_SERVICE_HOST
    : "http://localhost:3002/api",
  roomServiceHost: process.env.ROOM_SERVICE_HOST
    ? process.env.ROOM_SERVICE_HOST
    : "http://localhost:3003/api",
};
module.exports = config;
