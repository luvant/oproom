const axios = require("axios");
const config = require("../config.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

/**
 * Get reservations by query
 */
const getReservations = async (req) => {
  var query = req.query;
  var result;

  // is normal user
  if (req.user.role == "user") {
    // Get all this user's reservations
    query.userId = req.user._id;
    result = await axios.get(`${config.ioServiceHost}/reservations/`, {
      params: query,
    });
  }

  // is op-room manager
  if (req.user.role == "manager") {
    // Get all rooms owned by this user
    let opRooms = await axios.get(`${config.roomServiceHost}/rooms/`, {
      params: { ownerId: req.user._id },
      headers: {
        Authorization: req.header("Authorization"),
      },
    });
    opRooms = opRooms.data;
    let roomIds = opRooms.map((data) => data._id);
    query.roomId = { $in: roomIds };
    query = JSON.stringify(query);

    // Get all room's reservations
    result = await axios.get(`${config.ioServiceHost}/reservations/query`, {
      params: { query: query },
    });
  }

  return result.data;
};

/**
 * Get reservation by id
 */
const getReservationById = async (id) => {
  let result = await axios.get(`${config.ioServiceHost}/reservations/${id}`);
  return result.data;
};

/**
 * Create a operation reservation
 */
const createReservation = async (body) => {
  let result = await axios.post(`${config.ioServiceHost}/reservations`, body);
  return result.data;
};

/**
 * Update reservation
 */
const updateReservation = async (id, body) => {
  let result = await axios.put(
    `${config.ioServiceHost}/reservations/${id}`,
    body
  );
  return result.data;
};

/**
 * Delete reservation
 */
const deleteReservation = async (id) => {
  let result = await axios.delete(
    `${config.ioServiceHost}/reservations/${id}`,
    body
  );
  return result.data;
};

module.exports = {
  createReservation,
  getReservations,
  getReservationById,
  updateReservation,
  deleteReservation,
};
