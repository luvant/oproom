const { body, validationResult } = require("express-validator");
const axios = require("axios");
const config = require("../config.js");
const validationRules = (type) => {
  return [
    body("endDate").custom(async (value, { req }) => {
      let startDate = new Date(req.body.startDate);
      let endDate = new Date(value);
      if (endDate <= startDate) {
        throw new Error("End date of lab must be valid and after start date");
      }

      let query;

      // Check available start date and end date for reservation
      if (type == "create") {
        query = {
          $or: [
            {
              startDate: { $lte: startDate },
              endDate: { $gte: startDate },
            },
            {
              startDate: { $lte: endDate },
              endDate: { $gte: endDate },
            },
            {
              startDate: { $gt: startDate },
              endDate: { $lt: endDate },
            },
          ],
        };
      }
      if (type == "update") {
        query = {
          $or: [
            {
              startDate: { $lte: startDate },
              endDate: { $gte: startDate },
            },
            {
              startDate: { $lte: endDate },
              endDate: { $gte: endDate },
            },
            {
              startDate: { $gt: startDate },
              endDate: { $lt: endDate },
            },
          ],
          _id: {
            $ne: req.params.id,
          },
        };
      }

      query = JSON.stringify(query);

      // Check all reservations during this time
      let result = await axios.get(
        `${config.ioServiceHost}/reservations/query`,
        {
          params: { query: query },
        }
      );

      if (result.data.length > 0) {
        throw new Error("The reservation time is not available");
      }

      return true;
    }),
  ];
};

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(422).json({
    errors: extractedErrors,
  });
};

module.exports = {
  validationRules,
  validate,
};
