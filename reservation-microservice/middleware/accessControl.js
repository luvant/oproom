const config = require("../config");
const { default: axios } = require("axios");

/**
 * Auth middleware
 */
const accessControl = () => async (req, res, next) => {
  try {
    let reservationId = req.params.id;
    // Get the reservation
    reservationData = await axios.get(
      `${config.ioServiceHost}/reservations/${reservationId}`
    );
    reservationData = reservationData.data;
    if (req.user.role == "user") {
      // Check if the reservation is owned by the user
      if (reservationData.userId !== req.user._id) {
        throw new Error();
      }
      next();
    } else if (req.user.role == "manager") {
      let roomId = reservationData.roomId;

      // Get the room data
      let roomData = await axios.get(`${config.ioServiceHost}/rooms/${roomId}`);
      roomData = roomData.data;

      // Check if the room of the reservation is owned by the manager
      if (roomData.ownerId !== req.user._id) {
        throw new Error();
      }
      next();
    } else {
      throw new Error();
    }
  } catch (error) {
    res.status(401).send({ error: "Not authorized to access this resource" });
  }
};
module.exports = accessControl;
