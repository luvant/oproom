const Router = require("express").Router();

const reservationController = require("../controllers/reservationController");

Router.use("/reservations", reservationController);

module.exports = Router;
