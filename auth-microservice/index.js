const express = require("express");
const routes = require("./routes");
const config = require("./config");
const app = express();
const cors = require("cors");
const { handleError } = require("./errors");
const port = config.port;

app.use(cors());
app.use(express.json());

app.use("/api", routes);

app.use(handleError);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
