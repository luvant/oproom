const Router = require("express").Router();

const authController = require("../controllers/auth");

Router.use("/auth", authController);

module.exports = Router;
