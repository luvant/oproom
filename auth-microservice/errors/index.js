const handleError = (error, req, res, next) => {
  if (error.isAxiosError) {
    res.status(error.response.status).send(error.response.data);
  } else {
    res.status(500).send(error.message);
  }
};

module.exports = {
  handleError,
};
