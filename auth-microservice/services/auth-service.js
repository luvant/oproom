const axios = require("axios");
const config = require("../config.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

/**
 * Login 
 */
const login = async (email, password) => {
  let res = await axios.get(`${config.ioServiceHost}/users`, {
    params: { email },
  });
  if (res.data.length === 0) throw new Error("User is not exist");

  // Get user information
  let user = res.data[0];

  // Verification password
  const isPasswordMatch = await bcrypt.compare(password, user.password);
  if (!isPasswordMatch) throw new Error("Password is incorrect");

  // Generate token
  const token = jwt.sign({ _id: user._id }, config.jwtKey);

  // Add new token to db
  res = await axios.post(`${config.ioServiceHost}/users/token/${user._id}`, {
    token,
  });

  return {
    user: res.data,
    token: token,
  };
};

/**
 * Logout
 */
const logout = async (token) => {
  const data = jwt.verify(token, config.jwtKey);
  
  // delete user's token
  let res = await axios.delete(`${config.ioServiceHost}/users/token/${data._id}`, {
    data: {
      token,
    },
  });

  return res;
};

/** Sign Up */
const signUp = async (data) => {
  let res = await axios.post(`${config.ioServiceHost}/users/`, data);
  return res.data;
};

/**
 * Get User By Token
 */
const getUserByToken = async (token) => {
  const data = jwt.verify(token, config.jwtKey);
  let res = await axios.get(`${config.ioServiceHost}/users/`, {
    params: { _id: data._id, "tokens.token": token },
  });
  if (res.data.length === 0) throw new Error();
  return res.data[0];
};

module.exports = {
  login,
  getUserByToken,
  logout,
  signUp,
};
