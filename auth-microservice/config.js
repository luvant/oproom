const config = {
  jwtKey: process.env.JWT_KEY ? process.env.JWT_KEY : "junjun_10_08",
  port: process.env.EXPOSE_PORT ? process.env.EXPOSE_PORT : "3002",
  ioServiceHost: process.env.IO_SERVICE_HOST
    ? process.env.IO_SERVICE_HOST
    : "http://localhost:3001/api",
};
module.exports = config;
