const express = require("express");
const router = express.Router();
const {
  login,
  getUserByToken,
  logout,
  signUp,
} = require("../services/auth-service");

/**
 * Create a new user
 */
router.post("/", async (req, res, next) => {
  try {
    let data = await signUp(req.body);
    res.send(data);
  } catch (error) {
    next(error);
  }
});

/**
 * Login a registered user
 */
router.post("/login", async (req, res, next) => {
  try {
    let email = req.body.email;
    let password = req.body.password;
    let data = await login(email, password);
    res.send(data);
  } catch (error) {
    next(error);
  }
});

/**
 * Get user by token
 */
router.get("/me", async (req, res, next) => {
  try {
    const token = req.header("Authorization").replace("Bearer ", "");
    let data = await getUserByToken(token);
    res.send(data);
  } catch (error) {
    next(error);
  }
});

/**
 * Log user out of the application
 */
router.post("/logout", async (req, res, next) => {
  try {
    const token = req.header("Authorization").replace("Bearer ", "");
    await logout(token);
    res.send();
  } catch (error) {
    next(error);
  }
});

module.exports = router;
