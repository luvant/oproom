const mongoose = require("mongoose");
const config = require("../config");
mongoose.connect(config.dbUrl, {
  useNewUrlParser: true,
  useCreateIndex: true,
});
