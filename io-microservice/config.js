const { getSecret } = require("docker-secret");
const config = {
  dbUrl:
    process.env.NODE_ENV == "development"
      ? "mongodb://localhost:27017"
      : `mongodb://${getSecret(process.env.MONGO_USERNAME_FILE)}:${getSecret(
          process.env.MONGO_PASSWORD_FILE
        )}@${process.env.DB_HOST}?authSource=${getSecret(
          process.env.MONGO_USERNAME_FILE
        )}`,
  port: process.env.EXPOSE_PORT ? process.env.EXPOSE_PORT : "3001",
};
module.exports = config;
