const express = require("express");
const User = require("../models/User");
const router = express.Router();

/**
 * Create a new user
 */
router.post("/", async (req, res) => {
  try {
    const user = new User(req.body);
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});


router.get("/", async (req, res) => {
  try {
    let users = await User.find({
      ...req.query,
    }).sort({
      createdAt: "desc",
    });

    res.send(users);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Update board
 */
router.put("/:id", async (req, res) => {
  try {
    let id = req.params.id;
    let board = await User.findByIdAndUpdate(id, req.body, { new: true });
    res.send(board);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/**
 * Add new token
 */
router.post("/token/:id", async (req, res) => {
  try {
    let id = req.params.id;
    let token = req.body.token;
    let board = await User.findByIdAndUpdate(
      id,
      { $push: { tokens: { token } } },
      { new: true }
    );
    res.send(board);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/**
 * Remove token
 */
router.delete("/token/:id", async (req, res) => {
  try {
    let _id = req.params.id;
    let token = req.body.token;
    const user = await User.findOne({ _id, "tokens.token": token });
    user.tokens = user.tokens.filter((_token) => {
      return _token.token != token;
    });
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

module.exports = router;
