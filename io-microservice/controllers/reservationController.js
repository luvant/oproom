const express = require("express");
const Reservation = require("../models/Reservation");
const router = express.Router();

/**
 * Get reservation by query
 */
router.get("/", async (req, res) => {
  try {
    let reservations = await Reservation.find(req.query).sort({
      startDate: "desc",
    });

    res.send(reservations);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Get by mongo query string
 */
router.get("/query", async (req, res) => {
  try {
    let reservations = await Reservation.find(JSON.parse(req.query.query)).sort(
      {
        startDate: "desc",
      }
    );

    res.send(reservations);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Get reservation by Id
 */
router.get("/:id", async (req, res) => {
  try {
    let reservation = await Reservation.findById(req.params.id);
    res.send(reservation);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Create reservation
 */
router.post("/", async (req, res) => {
  try {
    let roomId = req.body.roomId;
    let userId = req.body.userId;
    let startDate = req.body.startDate;
    let endDate = req.body.endDate;

    let reservation = new Reservation({
      roomId,
      userId,
      startDate,
      endDate,
    });
    await reservation.save();
    res.send(reservation);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Update reservation
 */
router.put("/:id", async (req, res) => {
  try {
    let id = req.params.id;
    let reservation = await Reservation.findByIdAndUpdate(id, req.body, {
      new: true,
    });
    res.send(reservation);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/**
 * Delete reservation
 */
router.delete("/:id", async (req, res) => {
  try {
    let _id = req.params.id;
    let delRes = await Reservation.deleteOne({ _id });
    res.send(delRes);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

module.exports = router;
