const express = require("express");
const OpRoom = require("../models/OpRoom");
const router = express.Router();

/**
 * Get room by query
 */
router.get("/", async (req, res) => {
  try {
    let opRooms = await OpRoom.find(req.query).sort({
      createdAt: "desc",
    });

    res.send(opRooms);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Get room by mongo query string
 */
router.get("/", async (req, res) => {
  try {
    let opRooms = await OpRoom.find(JSON.parse(req.query.query)).sort({
      createdAt: "desc",
    });

    res.send(opRooms);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Get room by Id
 */
router.get("/:id", async (req, res) => {
  try {
    let opRoom = await OpRoom.findById(req.params.id);
    res.send(opRoom);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Create room
 */
router.post("/", async (req, res) => {
  try {
    let ownerId = req.body.ownerId;
    let name = req.body.name;
    let address = req.body.address;
    let opRoom = new OpRoom({
      name,
      address,
      ownerId,
    });
    await opRoom.save();
    res.send(opRoom);
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * Update room
 */
router.put("/:id", async (req, res) => {
  try {
    let id = req.params.id;
    let opRoom = await OpRoom.findByIdAndUpdate(id, req.body, { new: true });
    res.send(opRoom);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/**
 * Delete room
 */
router.delete("/:id", async (req, res) => {
  try {
    let _id = req.params.id;
    let delRes = await OpRoom.deleteOne({ _id });
    res.send(delRes);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

module.exports = router;
