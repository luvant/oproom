const express = require("express");
const User = require("../models/User");
const router = express.Router();

router.post('/', async (req, res) => {
    try {
        let userId = req.body.userId;
        let notification = req.body.notification;
        let board = await User.findByIdAndUpdate(
          userId,
          { $push: { notification: { notification } } },
          { new: true }
        );
        res.send(board);
      } catch (error) {
        res.status(400).send({ error: error.message });
      }
});