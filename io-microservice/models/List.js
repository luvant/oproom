const mongoose = require("mongoose");

const listSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
      minLength: 3,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      minLength: 7,
    },
    userId: {
      type: String,
      required: true,
    },
    boardId: {
      type: String,
      required: true,
    },
  },
  { timestamps: {} }
);

listSchema.statics.getAllListsByUser = async (boardId) => {
  return await List.find({ boardId: boardId }).sort({ createdAt: "asc" });
};

const List = mongoose.model("List", listSchema);

module.exports = List;
