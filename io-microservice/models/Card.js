const mongoose = require("mongoose");

const cardSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
      minLength: 3,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      minLength: 7,
    },
    userId: {
      type: String,
      required: true,
    },
    listId: {
      type: String,
      required: true,
    },
  },
  { timestamps: {} }
);

const Card = mongoose.model("Card", cardSchema);

module.exports = Card;
