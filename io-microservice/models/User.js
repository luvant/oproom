const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const config = require("../config");
const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    min: 3,
    max: 128,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    min: 3,
    max: 128,
  },
  password: {
    type: String,
    required: true,
    min: 3,
    max: 128,
  },
  tokens: [
    {
      token: {
        type: String,
        required: true,
      },
    },
  ],
  role: {
    type: String,
    validate: {
      validator: function (v) {
        return ["manager", "user"].includes(v);
      },
      message: (props) => `${props.value} is not a valid role`,
    },
  },
  notifications: [
    {
      date: {
        type: String,
        default: new Date().toLocaleString(),
      },
      content: {
        type: String,
        min: 3,
        max: 512,
      },
    },
  ],
});

userSchema.pre("save", async function (next) {
  // Hash the password before saving the user model
  const user = this;
  if (user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

const User = mongoose.model("User", userSchema);

module.exports = User;
