const mongoose = require("mongoose");

const opRoomSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      min: 3,
      max: 128,
      unique: true,
    },
    address: {
      type: String,
      required: true,
      trim: true,
      min: 3,
      max: 128,
    },
    ownerId: {
      type: String,
      required: true,
    },
  },
  { timestamps: {} }
);

opRoomSchema.statics.getAllBoardsByUser = async (userId) => {
  return await Board.find({ userId: userId }).sort({ createdAt: "desc" });
};

const OpRoom = mongoose.model("OpRoom", opRoomSchema);

module.exports = OpRoom;
