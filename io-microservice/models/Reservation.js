const mongoose = require("mongoose");

const reservationSchema = mongoose.Schema(
  {
    roomId: {
      type: String,
      required: true,
      trim: true,
    },
    userId: {
      type: String,
      required: true,
      trim: true,
    },
    startDate: {
      type: Date,
      required: true,
      trim: true,
    },
    endDate: {
      type: Date,
      required: true,
      trim: true,
    },
  },
  { timestamps: {} }
);

reservationSchema.index({ roomId: 1, userId: 1, startDate: 1, endDate: 1 },{ unique: true }); // schema level

const Reservation = mongoose.model("Reservation", reservationSchema);

module.exports = Reservation;
