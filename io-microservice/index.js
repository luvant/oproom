const express = require("express");
const routes = require("./routes");
require("./db/db");
const config = require("./config");
const app = express();
const cors = require("cors");

app.use(cors());
app.use(express.json());

app.use("/api", routes);

const port = config.port;

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
