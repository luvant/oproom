const Router = require("express").Router();

const userController = require("../controllers/userController");
const opRoomController = require("../controllers/opRoomController");
const reservationController = require("../controllers/reservationController")

Router.use("/users", userController);
Router.use("/rooms", opRoomController);
Router.use("/reservations", reservationController);

module.exports = Router;
